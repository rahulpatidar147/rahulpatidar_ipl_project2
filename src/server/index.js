const csv = require("csvtojson");
const fileSystem = require("fs");
const iplFunction=require('./ipl.js')
const csvMatchesFilePath = "../data/matches.csv";
const csvDeliveriesPath = "../data/deliveries.csv";

const csvToJson = (csvMatchesFilePath, csvDeliveriesPath) => {
    csv().fromFile(csvMatchesFilePath).then((matches)=>{
       csv().fromFile(csvDeliveriesPath).then((deliveries)=>{
         const sessionMatchYear= iplFunction.sessionMatch(matches);
         filePath = "../public/output/sessionMatchYear.json" 
         writeJson(filePath,sessionMatchYear);

         const wonMatch= iplFunction.matchWon(matches);
         filePath = "../public/output/matchesWonPerYear.json" 
         writeJson(filePath,wonMatch);

         const run= iplFunction.extraRun(matches,deliveries);
         filePath = "../public/output/extraRun.json" 
         writeJson(filePath,run);

         const eco= iplFunction.economicalBowlers(matches,deliveries);
         filePath = "../public/output/topEconomicalBowler.json" 
         writeJson(filePath,eco);

         const pro1= iplFunction.problem1(matches);
         filePath = "../public/output/wonTheTossWonTheMatch.json" 
         writeJson(filePath,pro1);

         const pro2= iplFunction.problem2(matches);
         filePath = "../public/output/playerOfMatch.json" 
         writeJson(filePath,pro2);

         const pro3= iplFunction.problem3(matches,deliveries);
         filePath = "../public/output/extraProblem3.json" 
         writeJson(filePath,pro3);

         const pro4= iplFunction.problem4(deliveries);
         filePath = "../public/output/extraProblem4.json" 
         writeJson(filePath,pro4);

         const pro5= iplFunction.problem5(deliveries);
         filePath = "../public/output/extraProblem5.json" 
         writeJson(filePath,pro5);

         function writeJson(filePath,fileName){
             fileSystem.writeFile(filePath, JSON.stringify(fileName), (error) =>{
                 if(error) console.log(error);
             })
         }
        })
    });
};

csvToJson(csvMatchesFilePath, csvDeliveriesPath);