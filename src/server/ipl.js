//1. Number of matches played per year for all the years in IPL.
function sessionMatch(iplMatchData){
let session=iplMatchData.reduce(function (previousValue, currentValue) {
    if(!previousValue[currentValue.season]){
      previousValue[currentValue.season] = 1;
    }
    else{
      previousValue[currentValue.season]+=1
    }
  return previousValue;
 }, {});
 return session;
}

//2. Number of matches won per team per year in IPL.
function matchWon(iplMatchData) {
let matchWonPerTeam={};
const result1 = (season)=>{
    let winnerTeams ={};
    for (let obj in iplMatchData){
        if (iplMatchData[obj]["season"] === season){
          if(!winnerTeams.hasOwnProperty(iplMatchData[obj]["winner"]))
            winnerTeams[iplMatchData[obj]["winner"]] = 1;
          else
            winnerTeams[iplMatchData[obj]["winner"]] += 1;
        }
      }
      return winnerTeams;
    }
 for(let obj in iplMatchData){
    if (!matchWonPerTeam.hasOwnProperty(iplMatchData[obj]["season"]))
    matchWonPerTeam[iplMatchData[obj]["season"]] = result1(iplMatchData[obj]["season"]);
 } 
 return matchWonPerTeam;
}

//3. Extra runs conceded per team in the year 2016
function extraRun(iplMatchData,deliveries){
  let matchId =[];
  for (let obj in iplMatchData){
    if(iplMatchData[obj].season == 2016){
      matchId.push(iplMatchData[obj].id);
    }
  }
   let extra_Run=deliveries.reduce(function (previousValue, currentValue) {
   let delivereId = currentValue.match_id;
   if(matchId.includes(delivereId)){
     if(!previousValue[currentValue.bowling_team]){
       previousValue[currentValue.bowling_team]=parseInt(currentValue.extra_runs);
     }
     else{
       previousValue[currentValue.bowling_team]+=parseInt(currentValue.extra_runs)
     }
   }
   return previousValue;
  }, {});
  return matchId;
}


//4. Top 10 economical bowlers in the year 2015
function economicalBowlers(iplMatchData,deliveries){
  let matchId =[];
  let economyArray=[];
  let topEconomical=[];
  for (let obj in iplMatchData){
    if(iplMatchData[obj].season == 2015){
      matchId.push(iplMatchData[obj].id);
    }
  }
   let totalRun=deliveries.reduce(function (previousValue, currentValue) {
   let delivereId = currentValue.match_id;
   if(matchId.includes(delivereId)){
     if(!previousValue[currentValue.bowler]){
          previousValue[currentValue.bowler]={
              total_runs:parseInt(currentValue.total_runs),
              ball:1,
          }
         
     }
     else{
       previousValue[currentValue.bowler].total_runs+=parseInt(currentValue.total_runs);
       previousValue[currentValue.bowler].ball+=1;
       
     }
   }
   return previousValue;
  }, {});
  for(let key in totalRun){
      let economy={};
     let over=(totalRun[key].ball/6);
     let eco=parseFloat(totalRun[key].total_runs/over);
     economy.name=`${key}`;
     economy.eco=eco;
     economyArray.push(economy);
   }
   economyArray.sort((a, b) => (a.eco> b.eco) ? 1 : -1)
  for (let index = 0; index< 10; index++){
    topEconomical.push(economyArray[index]);
  }
 return topEconomical;
}




//For folks who had previously solved the IPL questions in Propel / MountBlue Prep, you must solve the following questions instead:

//1. Find the number of times each team won the toss and also won the match.

function problem1(iplMatchData){
    let winnerTeams ={}; 
    for (let obj in iplMatchData){
            if(!winnerTeams.hasOwnProperty(iplMatchData[obj]["winner"])){
              if(iplMatchData[obj]["winner"]===iplMatchData[obj]["toss_winner"])
                 winnerTeams[iplMatchData[obj]["winner"]] = 1;
            }
            else{
              if(iplMatchData[obj]["winner"]===iplMatchData[obj]["toss_winner"])
                 winnerTeams[iplMatchData[obj]["winner"]] += 1;
            }
    }
  return winnerTeams;  
}
//2. Find a player who has won the highest number of Player of the Match awards for each season
function problem2(iplMatchData) {
  let playerOfTheMatch={};
  let playerOfTheMatchSession ={};
  const result1 = (season)=>{
      let winnerTeams ={};
      for (let obj in iplMatchData){
          if (iplMatchData[obj]["season"] === season){
            if(!winnerTeams.hasOwnProperty(iplMatchData[obj]["player_of_match"]))
              winnerTeams[iplMatchData[obj]["player_of_match"]] = 1;
            else
              winnerTeams[iplMatchData[obj]["player_of_match"]] += 1;
          }
        }
        return winnerTeams;
      }
   for(let obj in iplMatchData){
      if (!playerOfTheMatch.hasOwnProperty(iplMatchData[obj]["season"]))
      playerOfTheMatch[iplMatchData[obj]["season"]] = result1(iplMatchData[obj]["season"]);
   } 
   let maxPlayerOfMatch =[]

    for (let season in playerOfTheMatch){

        let max = 0

        for (let key in playerOfTheMatch[season]){

            if (playerOfTheMatch[season][key]>max){

                max = playerOfTheMatch[season][key]
                maxPlayerOfMatch = [key,max]
            
            }
        }

        playerOfTheMatchSession[season]={ }
        playerOfTheMatchSession[season][maxPlayerOfMatch[0]]=maxPlayerOfMatch[1]
       
    }
   return playerOfTheMatchSession;
  
};

//3. Find the strike rate of a batsman for each season
function problem3(iplMatchData,deliveries){
  function strikerRate(seasonIpl){
  let matchId =[];
  for (let obj in iplMatchData){
      if(iplMatchData[obj].season == seasonIpl)
        matchId.push(iplMatchData[obj].id);
  } 
  let batsman_run=deliveries.reduce(function (previousValue, currentValue) {
      let delivereId = currentValue.match_id;
      if(matchId.includes(delivereId)){
          if(!previousValue[currentValue.batsman]){
              previousValue[currentValue.batsman]={
                batsman_runs:parseInt(currentValue.batsman_runs),
                  ball:1,
                  strikeRateOfBatsman:0,
              }
             
         }
         else{
           previousValue[currentValue.batsman].batsman_runs+=parseInt(currentValue.batsman_runs);
           previousValue[currentValue.batsman].ball+=1;
           previousValue[currentValue.batsman].strikeRateOfBatsman=Math.round((previousValue[currentValue.batsman].batsman_runs/previousValue[currentValue.batsman].ball)*100);
           
         }
         
      }
      return previousValue;
     }, {});
  return batsman_run;
  }
  let strikeRateBatsman={};
  for(let obj in iplMatchData){
      if (!strikeRateBatsman.hasOwnProperty(iplMatchData[obj]["season"]))
      strikeRateBatsman[iplMatchData[obj]["season"]]=strikerRate(iplMatchData[obj]["season"]);
   }  
   
   
  return strikeRateBatsman;
  }

//4. Find the highest number of times one player has been dismissed by another player

function problem4(deliveries) {

  let dismissal ={}
  let highestdismissal ={}

  for (let delivery in deliveries){

      if (deliveries[delivery]['player_dismissed']){
     
          if (deliveries[delivery]['player_dismissed'] in dismissal){

              if (deliveries[delivery]['bowler'] in dismissal[deliveries[delivery]['player_dismissed']]){

                  dismissal[deliveries[delivery]['player_dismissed']][deliveries[delivery]['bowler']]= dismissal[deliveries[delivery]['player_dismissed']][deliveries[delivery]['bowler']]+1

              }else{

              dismissal[deliveries[delivery]['player_dismissed']][deliveries[delivery]['bowler']]=1
              }                 

          }else{

          dismissal[deliveries[delivery]['player_dismissed']]= {}
          dismissal[deliveries[delivery]['player_dismissed']][deliveries[delivery]['bowler']]=1

          }

      }
  }

  let maxPlayer =[]

  for (let batsman in dismissal){

      let max = 0

      for (let bowler in dismissal[batsman]){

          if (dismissal[batsman][bowler]>max){

              max = dismissal[batsman][bowler]
              maxPlayer = [[bowler,max]]
          
          }else if (dismissal[batsman][bowler]=max){

              maxPlayer.push([bowler,max])

          }
      }

      highestdismissal[batsman]={ }
      for (let index in maxPlayer){
          
      highestdismissal[batsman][maxPlayer[index][0]]=maxPlayer[index][1]

      }
     
  }
  return highestdismissal
}

//5. Find the bowler with the best economy in super overs

function problem5(deliveries){
  let superOver ={}; 
  for (let obj in deliveries){
    if(deliveries[obj]["is_super_over"]=='1'){
          if(!superOver.hasOwnProperty(deliveries[obj]["bowler"])){
            superOver[deliveries[obj]['bowler']]={
              total_runs:parseInt(deliveries[obj]['total_runs']),
              ball:1,
          }
               
          }
          else{
              superOver[deliveries[obj]['bowler']].total_runs+=parseInt(deliveries[obj]['total_runs']);
              superOver[deliveries[obj]['bowler']].ball+=1;
          }
        }     
  }
  
  let economyArray=[];
  for(let key in superOver){
    let economy={};
   let over=(superOver[key].ball/6);
   let eco=parseFloat(superOver[key].total_runs/over);
   economy.name=`${key}`;
   economy.eco=eco;
   economyArray.push(economy);
 }
 economyArray.sort((a, b) => (a.eco> b.eco) ? 1 : -1)

  
return economyArray[0];  
}  

module.exports= {matchWon,sessionMatch,extraRun,economicalBowlers,problem1,problem2,problem3,problem4,problem5};


